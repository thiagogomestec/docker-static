FROM nginx:latest

WORKDIR /usr/share/nginx/html

COPY index.html .

RUN mkdir static
COPY static static/

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]